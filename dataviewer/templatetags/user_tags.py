from django import template

register = template.Library()

@register.filter(name="has_group")
def has_group(user, group_name):
    if user.is_superuser or user.is_staff:
        return True
    try:
        group = Group.objects.get(name=group_name)
    except:
        return False  # group doesn't exist, so for sure the user isn't part of the group

    return user.groups.filter(name=group_name).exists()
