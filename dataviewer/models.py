import os
from django.db import models
from django.contrib.auth.models import Group
from django.template.defaultfilters import slugify
from django.conf import settings
from django.utils.crypto import get_random_string

""" 
Класс Post -- это модель для представления загруженных в систему файлов.
Поля модели:
    title - название загруженного файла
    upload_date - дата загрузки
    uploaded_by - пользователь, загрузивший файл
    file_body - сам файл
    belongs_to - группа(отдел), которой принадлежит файл
    tags - теги, по которым проще найти файл
"""
class Post(models.Model):
    def upload_path(instance, filename):
        file_as_list = filename.split('.')
        file_as_list[0] += ('_' + get_random_string(8))
        filename = '.'.join(file_as_list)
        return 'departments/{0}/{1}'.format(instance.belongs_to, filename)

    def filename(self):
        return os.path.basename(self.file_body.name)
    title       = models.CharField(max_length = 256)
    upload_date = models.DateTimeField(auto_now = False, auto_now_add = True)
    uploaded_by = models.ForeignKey(settings.AUTH_USER_MODEL, default = 1)
    file_body   = models.FileField(upload_to = upload_path)
    belongs_to  = models.CharField(max_length = 256)

    tags = models.CharField(max_length = 256, blank = True)
    # представляет теги в виде списка
    def get_tag_list(self):
        if self.tags != '':
            return self.tags.split(',')
        else:
            return ''

    def __str__(self):
        return self.title
    def get_absolute_url(self):
        return "/data/files/"
