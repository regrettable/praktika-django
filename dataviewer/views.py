from django.shortcuts import render
from django.views import View
from django.views.generic.edit import UpdateView
from .forms import UploadForm, UpdateForm
from django.http import HttpResponseRedirect
from django.db.models import Q
from .models import Post
from django.contrib.auth.decorators import permission_required
from django.utils.decorators import method_decorator

# Декораторы login_required работают для всего приложения dataviewer, поэтому при объявлении классов не требуются.

""" 
Класс Files  -- это вид, отображаемый при просмотре пользователем доступных ему файлов и поиске через строку поиска или теги.
"""
class Files(View):
    template_name = 'dataviewer/files.html'
    def get(self, request):
        # Отображение только доступных пользователю файлов
        if request.user.is_staff or request.user.is_superuser:
            posts_to_display = Post.objects.all()
        else:
            all_user_groups = request.user.groups.all()
            q_objects = Q()
            for group in all_user_groups:
                q_objects |= Q(belongs_to__contains=group)
            posts_to_display = Post.objects.filter(q_objects)
        # Поиск
        query = request.GET.get('q')
        if query:
            posts_to_display = posts_to_display.filter(
                    Q(title__icontains=query) |
                    Q(tags__icontains=query) 
                    )
        tags = request.GET.get('t')
        if tags:
            posts_to_display = posts_to_display.filter(tags__icontains=tags)
        context = {
                'posts_to_display': posts_to_display,
                }
        return render(request, self.template_name, context)


""" 
Класс Upload -- это вид, отображаемый при загрузке файла в систему. Загруженный файл автоматически присваивается той группе, в которой находится пользователь, загрузивший его.
"""
@method_decorator(permission_required("dataviewer.add_post", raise_exception=True), name="dispatch")
class Upload(View):
    form_class = UploadForm
    template_name = 'dataviewer/upload.html'
    def post(self, request):
        form = UploadForm(request.POST, request.FILES)
        if form.is_valid():
            instance = form.save(commit = False)
            instance.uploaded_by = request.user
            # Если в будущем будет несколько групп, не являющихся отделами, всю эту систему нужно будет переделать
            instance.belongs_to = request.user.groups.all().exclude(name="Uploaders")[0].name
            if instance.title == '':
                instance.title = instance.filename()
            instance.save()
            return HttpResponseRedirect('/data/files')
        return render(request, self.template_name, {'form': form})
    def get(self, request):
        # Убрать предупреждения при первой загрузке с помощью or None
        form = UploadForm(request.POST or None, request.FILES or None)
        return render(request, self.template_name, {'form': form})

# @method_decorator(permission_required("dataviewer.change_post", raise_exception=True), name="dispatch")
# class Update(View):
    # form_class = UpdateForm
    # template_name = 'dataviewer/update.html'
    # def post(self, request, id=None):
        # instance = get_object_or_404(Post, id=id)
        # form = UpdateForm(request.POST or None)
        # if form.is_valid():
            # if instance.title == '':
                # instance.title = instance.filename()
            # instance.save()
            # return HttpResponseRedirect('/data/files')
        # return render(request, self.template_name, {'form': form, 'instance': instance})
    # def get(self, request, id):
        # form = UploadForm(request.POST or None)
        # return render(request, self.template_name, {'form': form})

@method_decorator(permission_required("dataviewer.change_post", raise_exception=True), name="dispatch")
class Update(UpdateView):
    model = Post
    form_class = UpdateForm
    def form_valid(self, form):
            form.instance.uploaded_by = self.request.user
            return super(UpdateView, self).form_valid(form)
