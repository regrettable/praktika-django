'''dataviewer URL Configuration

dataviewer отвечает за предоставление пользователю доступных ему документов
'''

from django.conf.urls import url, include
from . import views as PostViews
from django.views.generic import ListView
from .models import Post
from .forms import UploadForm


urlpatterns = [
    url(r'^files/$', PostViews.Files.as_view(template_name="dataviewer/files.html"), name='files'),   
    url(r'^upload/', PostViews.Upload.as_view(template_name="dataviewer/upload.html"), name='upload'),
    url(r'^update/(?P<pk>\d+)/$', PostViews.Update.as_view(template_name="dataviewer/update.html"), name='update'),
    ]
