from django.forms import ModelForm
from .models import Post

# Форма на основе модели Post

class UploadForm(ModelForm):
    class Meta:
        model = Post
        fields = ('title', 'file_body', 'tags', )
        labels = {
                'title': 'Название',
                'file_body': '',
                'tags': 'Добавить теги',
                }
    def __init__(self, *args, **kwargs):
        super(UploadForm, self).__init__(*args, **kwargs)
        self.fields['title'].required = False

class UpdateForm(ModelForm):
    class Meta:
        model = Post
        fields = ('title', 'tags', )
        labels = {
                'title': 'Изменить название',
                'tags': 'Изменить теги',
                }
    def __init__(self, *args, **kwargs):
        super(UpdateForm, self).__init__(*args, **kwargs)
        self.fields['title'].required = False
