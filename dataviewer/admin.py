from django.contrib import admin
from .models import Post

# Register your models here.
class PostAdmin(admin.ModelAdmin):
    list_display = ["__str__", "upload_date", "file_body", "belongs_to", "uploaded_by",]

admin.site.register(Post, PostAdmin)
