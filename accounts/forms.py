from django import forms
from django.contrib.auth.forms import UsernameField, AuthenticationForm


""" MyAuthForm полностью аналогичен AuthenticationForm,
за исключением полей с русским текстом."""
class MyAuthForm(AuthenticationForm):
    username = UsernameField(
        label=("Логин"),
        max_length=254,
        widget=forms.TextInput(attrs={'autofocus': True}),
    )
    password = forms.CharField(
        label=("Пароль"),
        strip=False,
        widget=forms.PasswordInput,
    )

    error_messages = {
        'invalid_login': (
            "Пожалуйста, проверьте правильность введённых логина и пароля. Заметьте, что "
            "оба поля чувствительны к регистру."
        ),
        'inactive': ("Данный аккаунт был деактивирован. Пожалуйста, обратитесь к администратору."),
    }

