"""accounts URL configuration
accounts отвечает за управление пользователями и состоит целиком из модели

"""
from django.conf.urls import url, include
from .forms import MyAuthForm
from django.contrib.auth.views import LoginView, LogoutView

urlpatterns = [
    url(r'^logout/', LogoutView.as_view(next_page = '/')),
    url(r'^$', LoginView.as_view(redirect_authenticated_user = True,
        template_name='accounts/login.html',
        authentication_form=MyAuthForm),
    name="auth"),
    ]
