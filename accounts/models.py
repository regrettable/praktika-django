from django.db import models
from django.contrib.auth.models import Group, Permission
from django.contrib.auth.decorators import user_passes_test

# Декоратор для проверки, состоит ли пользователь в списке групп
def group_required(*group_names, in_every_group=False):    
    def in_groups(u):       
        if u.is_authenticated():            
            if bool(u.groups.filter(name__in=group_names)) | u.is_superuser:
                return True        
            return False    
    return user_passes_test(in_groups)

# Группа пользователей, имеющая доступ к загрузке файлов для своего отдела
class Uploader(Group):
    pass

